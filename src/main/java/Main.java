import gov.weather.graphical.xml.DWMLgen.wsdl.ndfdXML_wsdl.NdfdXMLBindingStub;
import gov.weather.graphical.xml.DWMLgen.wsdl.ndfdXML_wsdl.NdfdXMLLocator;
import org.apache.axis.AxisFault;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

public class Main {
    static NdfdXMLBindingStub ndfdXMLBindingStub;

    static {
        try {
            NdfdXMLLocator locator = new NdfdXMLLocator();
            ndfdXMLBindingStub = new NdfdXMLBindingStub(new URL("https://graphical.weather.gov"), locator);
        } catch (AxisFault e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws RemoteException {
        ndfdXMLBindingStub.latLonListCityNames(new BigInteger("12"));
    }
}
